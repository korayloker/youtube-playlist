# Youtube Playlist

This is a Drupal 10 (works with 9 as well) module which fetches a given Playlist from Youtube and create a block with thumbnails and titles from the videos, linking them to the original content.

## Roadmap

I have edited too many times to try out different things, so it needs to be simplified. No need to ask API Key several times. 

Asking API key and Playlist ID from admin menu would be proper instead of hardcoding it to the module obviously.

I am using it for bilingual website with different lists for different languages, but I'm not sure if it helps anyone, so I removed that part.
