<?php 
namespace Drupal\youtube_playlist\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use GuzzleHttp\Client;

/**
 * Provides a block with YouTube thumbnails.
 *
 * @Block(
 *   id = "youtube_playlist",
 *   admin_label = @Translation("Youtube Playlist")
 * )
 */
class YoutubePlaylist extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    try {
      $playlistId = '***PLAYLISTIDHERE***';
      $videos = $this->generatePlaylistThumbnails($playlistIdEn);

      // If playlist is not available, fetch latest 3 videos of the channel as fallback
      if (empty($videos)) {
        $fallbackVideos = $this->getLatestChannelVideos('YOUR_CHANNEL_ID', 3);
        $videos = $this->processVideos($fallbackVideos);
      }

      return [
        '#theme' => 'youtube_playlist',
        '#envideos' => $videos,
      ];
    } catch (\Exception $e) {
      // Log the error or handle it accordingly
      \Drupal::logger('youtube_playlist')->error('Error occurred: @message', [
        '@message' => $e->getMessage(),
      ]);

      // Display a generic error message to the user
      return [
        '#markup' => $this->t('An error occurred while fetching YouTube playlist information. Please try again later.'),
      ];
    }
  }

  /**
   * Function to retrieve YouTube playlist information.
   *
   * @param string $playlistId
   *   The ID of the YouTube playlist.
   *
   * @return array
   *   Array containing video information.
   */
  private function generatePlaylistThumbnails($playlistId) {
    try {
      $videos = $this->getYouTubePlaylistVideos($playlistId);
      $output = [];

      foreach ($videos as $video) {
        $videoId = $video['snippet']['resourceId']['videoId'];
        $videoDetails = $this->getVideoDetails($videoId);

        $output[] = [
          'videoId' => $videoId,
          'thumbnailUrl' => $videoDetails['thumbnailUrl'],
          'videoTitle' => $videoDetails['title'],
        ];
      }

      return $output;
    } catch (\Exception $e) {
      // Log the error or handle it accordingly
      \Drupal::logger('youtube_playlist')->error('Error occurred: @message', [
        '@message' => $e->getMessage(),
      ]);

      // Return an empty array to signify no videos found
      return [];
    }
  }

  /**
   * Function to retrieve YouTube playlist videos.
   *
   * @param string $playlistId
   *   The ID of the YouTube playlist.
   *
   * @return array
   *   Array containing video information.
   *
   * @throws \Exception
   */
  private function getYouTubePlaylistVideos($playlistId) {
    $apiKey = '***APIKEY***';
    $client = new Client();

    $response = $client->get("https://www.googleapis.com/youtube/v3/playlistItems?playlistId={$playlistId}&key={$apiKey}&part=snippet&maxResults=10");

    if ($response->getStatusCode() === 200) {
      $data = json_decode($response->getBody(), true);
      return $data['items'];
    } else {
      throw new \Exception('Failed to fetch YouTube playlist information.');
    }
  }

  /**
   * Function to retrieve video details.
   *
   * @param string $videoId
   *   The ID of the YouTube video.
   *
   * @return array
   *   Array containing video details.
   */
  private function getVideoDetails($videoId) {
    $apiKey = '***APIKEY***';
    $client = new Client();

    $response = $client->get("https://www.googleapis.com/youtube/v3/videos?id={$videoId}&key={$apiKey}&part=snippet");

    if ($response->getStatusCode() === 200) {
      $data = json_decode($response->getBody(), true);
      $snippet = $data['items'][0]['snippet'];

      return [
        'title' => $snippet['title'],
        'thumbnailUrl' => $snippet['thumbnails']['maxres']['url'],
      ];
    } else {
      // Return default values in case details cannot be fetched
      return [
        'title' => 'Video Title Not Available',
        'thumbnailUrl' => 'URL_To_Default_Thumbnail_Image',
      ];
    }
  }

  /**
   * Function to retrieve the latest videos from a YouTube channel.
   *
   * @param string $channelId
   *   The ID of the YouTube channel.
   * @param int $maxResults
   *   Maximum number of videos to retrieve.
   *
   * @return array
   *   Array containing video information.
   */
  private function getLatestChannelVideos($channelId, $maxResults) {
    try {
      $apiKey = '***APIKEY***';
      $client = new Client();

      $response = $client->get("https://www.googleapis.com/youtube/v3/search?key={$apiKey}&channelId={$channelId}&part=snippet,id&order=date&maxResults={$maxResults}");

      if ($response->getStatusCode() === 200) {
        $data = json_decode($response->getBody(), true);
        return $data['items'];
      } else {
        throw new \Exception('Failed to fetch latest videos from the channel.');
      }
    } catch (\Exception $e) {
      // Log the error or handle it accordingly
      \Drupal::logger('youtube_playlist')->error('Error occurred: @message', [
        '@message' => $e->getMessage(),
      ]);

      // Return an empty array to signify no videos found
      return [];
    }
  }

  /**
   * Processes the videos retrieved from the channel or playlist.
   *
   * @param array $videos
   *   Array containing video information.
   *
   * @return array
   *   Processed video information.
   */
  private function processVideos(array $videos) {
    $output = [];

    foreach ($videos as $video) {
      $videoId = $video['id']['videoId'];
      $videoDetails = $this->getVideoDetails($videoId);

      $output[] = [
        'videoId' => $videoId,
        'thumbnailUrl' => $videoDetails['thumbnailUrl'],
        'videoTitle' => $videoDetails['title'],
      ];
    }

    return $output;
  }
}
